import React from 'react';
import { Layout, Menu } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import ConnectFour from './pages/ConnectFour';
import AboutMe from './pages/AboutMe';
import Pathfinding from './pages/Pathfinding';
import Resume from './pages/Resume';
import GameOfLife from './pages/Conway';
import styles from './App.modules.scss';

const { Sider, Content } = Layout;
const menuItems = [
  ['Index', '/'],
  ['Resume', '/resume'],
  ['Pathfinding', '/pathfinding'],
  ['Connect Four', '/connectfour'],
  ['Game Of Life', '/conway'],
];

const App: React.FunctionComponent<{}> = () => (
  <Router>
    <Layout>
      <Sider>
        <Menu>
          {menuItems.map((item, index) => (
            <Menu.Item key={index}>
              {item[0]}
              <Link to={item[1]} />
            </Menu.Item>
          ))}
        </Menu>
      </Sider>
      <Content className={styles.content}>
        <Switch>
          <Route path="/connectfour">
            <ConnectFour />
          </Route>
          <Route path="/resume">
            <Resume />
          </Route>
          <Route path="/pathfinding">
            <Pathfinding />
          </Route>
          <Route path="/conway">
            <GameOfLife />
          </Route>
          <Route path="/">
            <AboutMe />
          </Route>
        </Switch>
      </Content>
    </Layout>
  </Router>
);

export default App;
