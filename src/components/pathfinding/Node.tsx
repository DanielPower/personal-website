import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { TILE_SIZE, TILE_PADDING } from './constants';

const Node: React.FunctionComponent<{
  x: number;
  y: number;
  color: string;
}> = ({ x, y, color }) => {
  const [isHovered, setIsHovered] = useState(false);
  return (
    <rect
      key={String(y * x + y)}
      x={x * (TILE_SIZE + TILE_PADDING) + TILE_PADDING}
      y={y * (TILE_SIZE + TILE_PADDING) + TILE_PADDING}
      width={TILE_SIZE}
      height={TILE_SIZE}
      fill={isHovered ? '#FFFFFF' : color}
      onMouseEnter={(): void => setIsHovered(true)}
      onMouseLeave={(): void => setIsHovered(false)}
    ></rect>
  );
};

Node.propTypes = {
  x: PropTypes.number,
  y: PropTypes.number,
  color: PropTypes.string,
};

export default Node;
