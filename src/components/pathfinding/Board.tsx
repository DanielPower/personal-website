import React from 'react';
import PropTypes from 'prop-types';
import Node from './Node';
import { TILE_SIZE, TILE_PADDING } from './constants';

const Board: React.FunctionComponent<{
  map: Array<Array<number>>;
  colors: Array<string>;
}> = ({ map, colors }) => {
  return (
    <svg
      width={(TILE_SIZE + TILE_PADDING) * map[0].length}
      height={(TILE_SIZE + TILE_PADDING) * map.length}
    >
      <rect
        x={0}
        y={0}
        width={(TILE_SIZE + TILE_PADDING) * map[0].length + TILE_PADDING}
        height={(TILE_SIZE + TILE_PADDING) * map.length + TILE_PADDING}
        fill="#000000"
      />
      {map.map((column, x) => (
        <>
          {column.map((node, y) => (
            <Node key={String(y * x + y)} x={x} y={y} color={colors[node]} />
          ))}
          <br />
        </>
      ))}
    </svg>
  );
};

Board.propTypes = {
  map: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number)).isRequired,
  colors: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default Board;
