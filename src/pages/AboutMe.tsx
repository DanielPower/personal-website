import React from 'react';

const AboutMe: React.FunctionComponent<{}> = () => (
  <>
    <h1>About Me</h1>
    <p>
      Hi, I&apos;m Dan, and I&apos;m a Full Stack Developer for&nbsp;
      <a href="https://www.colabsoftware.com">Colab Software</a>
    </p>
  </>
);

export default AboutMe;
