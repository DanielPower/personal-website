import React, { useState, useEffect, useCallback, useRef } from 'react';
import { Button } from 'antd';
const BOARD_WIDTH = 50;
const BOARD_HEIGHT = 50;
const NODE_WIDTH = 16;
const NODE_HEIGHT = 16;
const NODE_SPACING = 1;

const DIRECTIONS = [
  { x: -1, y: -1 },
  { x: 0, y: -1 },
  { x: 1, y: -1 },
  { x: -1, y: 0 },
  { x: 1, y: 0 },
  { x: -1, y: 1 },
  { x: 0, y: 1 },
  { x: 1, y: 1 },
];

const generateBoard = (fill: Function): Array<Array<boolean>> => {
  const array = new Array(BOARD_HEIGHT);
  for (let i = 0; i < BOARD_WIDTH; i += 1) {
    array[i] = new Array(BOARD_WIDTH);
    for (let j = 0; j < BOARD_HEIGHT; j += 1) {
      array[i][j] = fill();
    }
  }
  return array;
};

const inBounds = (x: number, y: number): boolean =>
  x < BOARD_WIDTH && x >= 0 && y < BOARD_HEIGHT && y >= 0;

const Pathfinding: React.FunctionComponent<{}> = () => {
  const [nodes, setNodes] = useState<Array<Array<boolean>>>(
    generateBoard(() => false),
  );
  const [isRunning, setIsRunning] = useState<boolean>(false);
  const isRunningRef = useRef(isRunning);
  isRunningRef.current = isRunning;
  const nodesRef = useRef(nodes);
  nodesRef.current = nodes;

  const toggleNode = (x: number, y: number): void => {
    const nodesCopy = [...nodes];
    nodesCopy[y][x] = !nodesCopy[y][x];
    setNodes(nodesCopy);
  };

  const toggleRunning = (): void => {
    setIsRunning(!isRunning);
  };

  const randomizeBoard = (): void => {
    setNodes(generateBoard(() => Boolean(Math.round(Math.random()))));
  };

  const reset = (): void => {
    setNodes(generateBoard(() => false));
  };

  const step = useCallback((): void => {
    const nodesCopy = generateBoard(() => false);
    for (let y = 0; y < BOARD_HEIGHT; y += 1) {
      for (let x = 0; x < BOARD_WIDTH; x += 1) {
        let neighborCount = 0;
        DIRECTIONS.forEach((direction) => {
          if (!inBounds(x + direction.x, y + direction.y)) {
            return;
          }
          if (nodesRef.current[y + direction.y][x + direction.x]) {
            neighborCount += 1;
          }
        });
        nodesCopy[y][x] =
          (nodesRef.current[y][x] && neighborCount === 2) ||
          neighborCount === 3;
      }
    }
    setNodes(nodesCopy);
  }, [nodes]);

  const run = (): void => {
    if (isRunningRef.current) {
      step();
      setTimeout(run, 100);
    }
  };

  useEffect((): void => {
    if (isRunning) {
      run();
    }
  }, [isRunning]);

  return (
    <>
      <h1>{"Conway's Game of Life"}</h1>
      <Button onClick={toggleRunning}>{isRunning ? 'Stop' : 'Start'}</Button>
      <Button onClick={step}>Step</Button>
      <Button onClick={reset}>Reset</Button>
      <Button onClick={randomizeBoard}>Random</Button>
      <br />
      <svg
        width={BOARD_WIDTH * (NODE_WIDTH + NODE_SPACING) + NODE_SPACING}
        height={BOARD_HEIGHT * (NODE_HEIGHT + NODE_SPACING) + NODE_SPACING}
      >
        {nodes.map((row, y) => (
          <>
            {row.map((alive, x) => (
              <rect
                key={`${x}-${y}`}
                x={x * (NODE_WIDTH + NODE_SPACING)}
                y={y * (NODE_HEIGHT + NODE_SPACING)}
                width={NODE_WIDTH}
                height={NODE_HEIGHT}
                fill={alive ? '#33CC33' : '#CC3333'}
                onClick={(): void => toggleNode(x, y)}
              />
            ))}
          </>
        ))}
      </svg>
    </>
  );
};

export default Pathfinding;
