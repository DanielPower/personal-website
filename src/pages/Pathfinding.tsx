import React from 'react';
import Board from '../components/pathfinding/Board';
import { davesMap } from '../components/pathfinding/maps/default';

const COLORS = ['#777777', '#00FF00', '#0000FF'];

const Pathfinding: React.FunctionComponent<{}> = () => {
  return (
    <>
      <h1>Pathfinding</h1>
      <Board map={davesMap} colors={COLORS} />
    </>
  );
};

export default Pathfinding;
